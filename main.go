package main

import (
	"fmt"
	"time"

	"git.liguros.net/palica/golang/go-intro-book/matrices/profile"
)

func displayMatrix(A [][]int, name string) {
	defer profile.Duration(time.Now(), "DisplayMatrix")
	fmt.Println("This is matrix", name, ":")
	for i := 0; i < len(A); i++ {
		fmt.Println(A[i])
	}
}

func describeMatrix(A [][]int, name string) {
	defer profile.Duration(time.Now(), "DescribeMatrix")

	fmt.Println("The properties of matrix", name, "are:")
	rows := len(A)
	columns := len(A[0])
	fmt.Println(" - number of rows", rows)
	fmt.Println(" - number of columns", columns)
}

func addMatrix(A, B [][]int) {
	defer profile.Duration(time.Now(), "AddMatrix")

	fmt.Println("Adding two matrices:")
	describeMatrix(A, "A")
	describeMatrix(B, "B")
	displayMatrix(A, "A")
	displayMatrix(B, "B")
	AB := [][]int{
		{A[0][0] + B[0][0], A[0][1] + B[0][1]},
		{A[1][0] + B[1][0], A[1][1] + B[1][1]},
	}
	fmt.Println("And this is A + B =")
	for i := 0; i < len(AB); i++ {
		fmt.Println(AB[i])
	}
}

func simpleAddition(x, y int) int {
	return x + y
}

func main() {
	fmt.Println("Hello let's do some matrix manipulations")
	matrixA := [][]int{
		[]int{3, 8},
		[]int{4, 6},
	}

	matrixB := [][]int{
		[]int{4, 0},
		[]int{1, -9},
	}

	// add matrices together
	addMatrix(matrixA, matrixB)
}
