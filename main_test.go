package main

import (
	"testing"
)

func Test_simpleAddition(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{"first", args{1, 3}, 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := simpleAddition(tt.args.x, tt.args.y); got != tt.want {
				t.Errorf("simpleAddition() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_addMatrix(t *testing.T) {
	type args struct {
		A [][]int
		B [][]int
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			addMatrix(tt.args.A, tt.args.B)
		})
	}
}
